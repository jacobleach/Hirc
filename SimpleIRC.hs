import Network
import System.IO
import Control.Concurrent
import Data.List
import System.Console.ANSI
import Control.Monad
import Data.IORef
import Data.Char (ord)

main = do
    inputChar <- newIORef ""   
    hSetBuffering stdin NoBuffering
    hSetBuffering stdout NoBuffering
    ircHandle <- connectTo "mccs.stu.marist.edu" (PortNumber (fromIntegral 6667))
    sendMessage ircHandle "NICK Hirc_Test"
    sendMessage ircHandle "USER Hirc_Test 0 * :Hirc"
    sendMessage ircHandle "JOIN :#chat1"
    forkIO (sendInput ircHandle "" inputChar)
    handleMessages ircHandle inputChar

sendInput handle buffer comm = do
    input <- hGetChar stdin
    let newBuffer = buffer ++ [input]
    if(input == '\n') then do
        writeIORef comm ""
        sendMessage handle buffer
        sendInput handle "" comm
    else if((ord input) == 8 || (ord input) == 127) then do
        hCursorBackward stdout 3
        putStr "   "
        hFlush stdout
        hCursorBackward stdout 3
        writeIORef comm (init buffer) 
        sendInput handle (init buffer) comm
    else do
        writeIORef comm newBuffer
        sendInput handle newBuffer comm

sendMessage :: Handle -> String -> IO ()
sendMessage writer message = do
    hPutStrLn writer message
    hFlush writer

handleMessages :: Handle -> IORef ([Char]) -> IO ()
handleMessages reader comm = do
    message <- hGetLine reader
    newBuffer <- readIORef comm
    if (take 6 message) == "PING :" then do
        let responce = "PONG :" ++ (drop 6 message)
        sendMessage reader responce
        putStrLn message
        putStrLn responce
    else if(newBuffer == "") then do
        putStrLn message    
    else do
        hClearLine stdout
        setCursorColumn 0 
        hPutStrLn stdout message
        hPutStr stdout newBuffer
        hFlush stdout
    handleMessages reader comm
