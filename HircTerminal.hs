module Hirc.Terminal
(
getChar,
handleInputChar
) where

import System.IO
import Data.IORef

data HircTerminal = 
    HircTerminal { input :: Handle, output :: Handle, inputBuffer :: IORef (String) }

newHircTerminal :: Handle -> Handle -> IO (HircTerminal)
newHircTerminal input output = do
    buffer <- newIORef ""
    return (HircTerminal input output buffer)

--Needs to be made more thread safe
tGetChar :: HircTerminal -> IO (Char)
tGetChar terminal = do 
    input <- hGetChar (input terminal)
    modifyIORef (inputBuffer terminal) (++ [input])
    return (input)

handleInputChar :: HircTerminal -> Char -> IO ()
handleInputChar terminal '\b' = do 
    hPutStr (output terminal) "\b\b\b"
    hFlush  (output terminal)

handleInputChar terminal char = do 
    return ()

tPutStr :: HircTerminal -> String -> IO ()
tPutStr terminal string = do
    hPutStr (output terminal) string
