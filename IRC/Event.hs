module IRC.Event
(
Event,
getNumericEvent
) where

import Data.List.Split
import Data.List (isPrefixOf, intercalate)
import IRC.User 
import IRC.Channel

type Servername = String
type WelcomeMessage = String
type ServerVersion = String
type CreationDate = String
type UserModes = String
type ChannelModes = String
type ServerSupport = String
type CommandNumeric = Integer
type Message = String
type PingServer = String

data UserHostNameList = NameList [User] deriving (Show)

userHostNameList :: [User] -> UserHostNameList
userHostNameList names
    | (length names) > 5 = error "Only 5 or less names allowed."
    | (length names) < 1 = error "Must have at least one name."
    | otherwise = NameList names

data Event = 
    RPL_WELCOME WelcomeMessage Nickname Username Hostname | 
    RPL_YOURHOST Servername ServerVersion |
    RPL_CREATED CreationDate |
    RPL_MYINFO Servername ServerVersion UserModes ChannelModes |
    ISUPPORT ServerSupport |
    RPL_USERHOST UserHostNameList | 
    JOIN Channel |
    PING PingServer |
    PONG PingServer |
    PRIVMSG Channel Message |
    QUIT Message 
    deriving (Show) 

getNumericEvent :: Integer -> String -> Event
getNumericEvent 001 params = RPL_WELCOME 
                                (intercalate " " (init splitSp))
                                (head nick) 
                                (head user)
                                (host)
                                where splitSp = splitOn " " params
                                      nick    = splitOn "!" (last splitSp)
                                      user    = splitOn "@" (head (tail nick))
                                      host    = head (tail user)

getNumericEvent 002 params = RPL_YOURHOST
                                (takeWhile (not . (== ',')) ((words params) !! 3))
                                (last (words params))

getNumericEvent 003 params = RPL_CREATED (last (words params))

getNumericEvent 004 params = RPL_MYINFO 
                                ((words params) !! 0)
                                ((words params) !! 1)
                                ((words params) !! 2)
                                ((words params) !! 3)
--TODO
getNumericEvent 005 params = ISUPPORT params 

--TODO Make + / - work!
getNumericEvent 302 params = RPL_USERHOST (userHostNameList (map (parsePrefix) (words removeFirst)))
                                where parsePrefix string = 
                                        (User 
                                            (takeWhile (not . (== '=')) string)
                                            (takeWhile (not . (== '@')) (head (tail (splitOn "=" string))))
                                            ""
                                            (last (splitOn "@" string))
                                        )
                                      removeFirst = tail params
