module IRC.Command
(
Command,
Parameter(),
parameter
) where

import Data.List (isInfixOf)

data Parameter = 
        Parameter String deriving (Show)

parameter string 
    | " " `isInfixOf` string    = error "Non-trailing parameters may not have spaces"
    | otherwise                 = Parameter string

type TrailingParameter = String

data OptionalParameter = 
        OptionalParameter Parameter |
        NoParameter

data OptionalTrailingParameter = 
        OptionalTrailingParameter TrailingParameter |
        NoTrailingParameter

data Command =
    PASS Parameter |
    USER Parameter Parameter Parameter |
    NICK Parameter |
    JOIN Parameter OptionalParameter |
    MODE Parameter Parameter |
    PRIVMSG Parameter TrailingParameter |
    QUIT OptionalTrailingParameter
