module IRC.Message
(
Message,
Prefix,
Parameter,
TrailingParameter,
ParameterList
) where

import Data.List (intercalate, isInfixOf, isPrefixOf)
import Data.List.Split
import IRC.User (Nickname, Username, Hostname)
import IRC.Event

type RawMessage = String
type Servername = String

data Message = 
    ResponceMessage Prefix Event |
    CommandMessage Event |

data Prefix = 
    UserPrefix Nickname Username Hostname |
    ServerPrefix Servername
    deriving (Show)

data Parameter = Parameter String deriving (Show)

parameter :: String -> Parameter
parameter string 
    | " " `isInfixOf` string    = error "Non-Trailing parameters may not have spaces."
    | otherwise                 = Parameter string

type TrailingParameter = String

data ParameterList = 
    List [Parameter] | 
    TrailingList [Parameter] TrailingParameter

parsePrefix :: String -> Prefix
parsePrefix prefixString 
    | not (":" `isPrefixOf` prefixString) = error "String is not a prefix"
    | not ("!" `isInfixOf` prefixString)  = ServerPrefix (tail prefixString)
    | otherwise = UserPrefix nick user host
        where nick    = tail (head splitEx)
              user    = head splitAt
              host    = head (tail splitAt)
              splitEx = splitOn "!" prefixString
              splitAt = splitOn "@" (head (tail splitEx))
{-
parse :: RawMessage -> Message
parse message 
    | ":" `isPrefixOf` message  = parseResponce message
    | otherwise                 = parseCommand message

parseResponce :: RawMessage -> Message
parseResponce message

parseCommand :: RawMessage -> Message
parseCommand message 
    | "JOIN" `isPrefixOf` message = CommandMessage (JOIN (Channel 
    -}
