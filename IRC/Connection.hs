module IRC.Connection
(
--data types
IRCServer (IRCServer),
IRCConnection,

--functions
connect
) where 

import System.IO
import Network (PortID, HostName, connectTo, PortNumber)

data IRCServer = IRCServer { host :: HostName, port :: PortID } deriving(Show)
data IRCConnection = IRCConnection { handle :: Handle }

--Establishes a connection to the IRC Server.
connect :: IRCServer -> IO (IRCConnection)
connect server = do 
    handle <- connectTo (host server) (port server)
    return (IRCConnection handle)

--Sends a raw message to the IRC Server. 
sendMessage :: IRCConnection -> String -> IO ()
sendMessage connection message = do
    hPutStrLn (handle connection) message
    hFlush (handle connection)

--Gets a raw message from the IRC Server if one exists, otherwise it blocks until there is one. 
getMessage :: IRCConnection -> IO (String)
getMessage connection = 
    hGetLine (handle connection)
