module IRC.User
(
User(User),
Nickname,
Username,
Realname,
Hostname
) where

type Nickname = String
type Username = String
type Realname = String
type Hostname = String

data User = User { nickName :: Nickname, userName :: Username, realName :: Realname, host :: Hostname }
                deriving (Show)
