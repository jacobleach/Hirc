module IRC.Channel
(
ChannelName(),
channelName,
Channel(Channel)
) where

import Data.List (isPrefixOf, isInfixOf, intercalate)
import Data.Char (chr)

nameLength = 3

data ChannelName = ChannelName String

channelName :: String -> ChannelName
channelName name 
    | " " `isInfixOf` name          = error "Channel name must not contain spaces."
    | "," `isInfixOf` name          = error "Channel name must not contain commas."
    | [(chr 7)] `isInfixOf` name    = error "Channel name must not contain ^G (ASCII 7)."
    | not ("#" `isPrefixOf` name)   = error "Channel name must begin with '#'."
    | (length name) > nameLength    = error "Channel name must be no longer than 50 characters."
    | otherwise                     = ChannelName name

instance Show ChannelName where
    show (ChannelName name) = name

data Channel = Channel { name :: ChannelName } deriving (Show)
